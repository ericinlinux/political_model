# Simulated Annealing and Political Positioning Model

The code intends to find parameters that adjust well on a cognitive model for political positioning.

________

We use 3 agents to simulate our model, and deliver a set of messages in sequence for the 3 agents. 


## Jan's messages

Eric,
 
Concerning the choice of steepness and threshold values, there is also another way that may be convenient. Then you set all thresholds on one value 0.14 and then try the steepness values from 0.5 (for four arrows), 0.7 (for three arrows),  1 (for two arrows) to 2.5 (for 1 arrow), for example. This is illustrated in Table 2.9 at page 76 of my book. In this way you approximate scaled sum functions.
 
Jan

Then the idea is to make aggimpact 0 if otherwise it would become negative: for example aggimpactY = MAX(0, alogistic(…)). Don’t make the state value Y(t+delta t) itself 0 when otherwise it would become negative; that would give artificial looking graphs.
 